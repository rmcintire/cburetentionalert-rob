﻿namespace CbuRetentionAlert.Test.Ui
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.cmdEmployeeId = new System.Windows.Forms.Button();
            this.cmdGetUserRoles = new System.Windows.Forms.Button();
            this.txtUser = new System.Windows.Forms.TextBox();
            this.cmdSemesterByCount = new System.Windows.Forms.Button();
            this.cmdCurrentSemSubsession = new System.Windows.Forms.Button();
            this.cmdFillAdmit = new System.Windows.Forms.Button();
            this.cmdE2Start = new System.Windows.Forms.Button();
            this.cmdReport = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(275, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Get Semester Dates";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(12, 41);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(275, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Get Instructor Courses";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(12, 70);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(275, 23);
            this.button3.TabIndex = 2;
            this.button3.Text = "Get Student Enrollments";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(12, 99);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(275, 23);
            this.button4.TabIndex = 3;
            this.button4.Text = "Get Course Info";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(12, 128);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(275, 23);
            this.button5.TabIndex = 4;
            this.button5.Text = "Get Instructor Info";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // cmdEmployeeId
            // 
            this.cmdEmployeeId.Location = new System.Drawing.Point(12, 157);
            this.cmdEmployeeId.Name = "cmdEmployeeId";
            this.cmdEmployeeId.Size = new System.Drawing.Size(275, 23);
            this.cmdEmployeeId.TabIndex = 5;
            this.cmdEmployeeId.Text = "Get Ad Employee Id";
            this.cmdEmployeeId.UseVisualStyleBackColor = true;
            this.cmdEmployeeId.Click += new System.EventHandler(this.cmdEmployeeId_Click);
            // 
            // cmdGetUserRoles
            // 
            this.cmdGetUserRoles.Location = new System.Drawing.Point(12, 186);
            this.cmdGetUserRoles.Name = "cmdGetUserRoles";
            this.cmdGetUserRoles.Size = new System.Drawing.Size(275, 23);
            this.cmdGetUserRoles.TabIndex = 6;
            this.cmdGetUserRoles.Text = "Get User Roles";
            this.cmdGetUserRoles.UseVisualStyleBackColor = true;
            this.cmdGetUserRoles.Click += new System.EventHandler(this.cmdGetUserRoles_Click);
            // 
            // txtUser
            // 
            this.txtUser.Location = new System.Drawing.Point(294, 188);
            this.txtUser.Name = "txtUser";
            this.txtUser.Size = new System.Drawing.Size(274, 20);
            this.txtUser.TabIndex = 7;
            // 
            // cmdSemesterByCount
            // 
            this.cmdSemesterByCount.Location = new System.Drawing.Point(294, 12);
            this.cmdSemesterByCount.Name = "cmdSemesterByCount";
            this.cmdSemesterByCount.Size = new System.Drawing.Size(275, 23);
            this.cmdSemesterByCount.TabIndex = 8;
            this.cmdSemesterByCount.Text = "Get Semester Dates By Count";
            this.cmdSemesterByCount.UseVisualStyleBackColor = true;
            this.cmdSemesterByCount.Click += new System.EventHandler(this.cmdSemesterByCount_Click);
            // 
            // cmdCurrentSemSubsession
            // 
            this.cmdCurrentSemSubsession.Location = new System.Drawing.Point(294, 41);
            this.cmdCurrentSemSubsession.Name = "cmdCurrentSemSubsession";
            this.cmdCurrentSemSubsession.Size = new System.Drawing.Size(275, 23);
            this.cmdCurrentSemSubsession.TabIndex = 9;
            this.cmdCurrentSemSubsession.Text = "Get Current Semester With Subsession";
            this.cmdCurrentSemSubsession.UseVisualStyleBackColor = true;
            this.cmdCurrentSemSubsession.Click += new System.EventHandler(this.cmdCurrentSemSubsession_Click);
            // 
            // cmdFillAdmit
            // 
            this.cmdFillAdmit.Location = new System.Drawing.Point(402, 369);
            this.cmdFillAdmit.Name = "cmdFillAdmit";
            this.cmdFillAdmit.Size = new System.Drawing.Size(275, 23);
            this.cmdFillAdmit.TabIndex = 10;
            this.cmdFillAdmit.Text = "Update Student Info with Admit Info";
            this.cmdFillAdmit.UseVisualStyleBackColor = true;
            this.cmdFillAdmit.Click += new System.EventHandler(this.cmdFillAdmit_Click);
            // 
            // cmdE2Start
            // 
            this.cmdE2Start.Location = new System.Drawing.Point(293, 70);
            this.cmdE2Start.Name = "cmdE2Start";
            this.cmdE2Start.Size = new System.Drawing.Size(275, 23);
            this.cmdE2Start.TabIndex = 11;
            this.cmdE2Start.Text = "Is E2 Start";
            this.cmdE2Start.UseVisualStyleBackColor = true;
            this.cmdE2Start.Click += new System.EventHandler(this.cmdE2Start_Click);
            // 
            // cmdReport
            // 
            this.cmdReport.Location = new System.Drawing.Point(293, 99);
            this.cmdReport.Name = "cmdReport";
            this.cmdReport.Size = new System.Drawing.Size(275, 23);
            this.cmdReport.TabIndex = 12;
            this.cmdReport.Text = "Run Report";
            this.cmdReport.UseVisualStyleBackColor = true;
            this.cmdReport.Click += new System.EventHandler(this.cmdReport_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(689, 404);
            this.Controls.Add(this.cmdReport);
            this.Controls.Add(this.cmdE2Start);
            this.Controls.Add(this.cmdFillAdmit);
            this.Controls.Add(this.cmdCurrentSemSubsession);
            this.Controls.Add(this.cmdSemesterByCount);
            this.Controls.Add(this.txtUser);
            this.Controls.Add(this.cmdGetUserRoles);
            this.Controls.Add(this.cmdEmployeeId);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button cmdEmployeeId;
        private System.Windows.Forms.Button cmdGetUserRoles;
        private System.Windows.Forms.TextBox txtUser;
        private System.Windows.Forms.Button cmdSemesterByCount;
        private System.Windows.Forms.Button cmdCurrentSemSubsession;
        private System.Windows.Forms.Button cmdFillAdmit;
        private System.Windows.Forms.Button cmdE2Start;
        private System.Windows.Forms.Button cmdReport;
    }
}

