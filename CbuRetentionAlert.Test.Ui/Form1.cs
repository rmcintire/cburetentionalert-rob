﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CbuRetentionAlert.Lib.Repository;

namespace CbuRetentionAlert.Test.Ui
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var dates = CbuCx.Lib.Academics.AcademicCalendarModel.GetUndergradSemesterDates();

            var currentSemester = CbuCx.Lib.Academics.AcademicCalendarModel.GetCurrentSemester();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var courses = CbuCx.Lib.Academics.CourseEnrollmentModel.GetFacultyCoursesForTheCurrentSemester(53359);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var enrollments = CbuCx.Lib.Academics.CourseEnrollmentModel.GetStudentEnrollmentsForCourse("CIS265", "AE", "SP", 2013);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var course = CbuCx.Lib.Academics.CourseModel.GetCourse("KIN332", "AE", "SP", 2013);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            var instructor = CbuCx.Lib.User.FacultyUser.GetFacultyInfo(212325);
        }

        private void cmdEmployeeId_Click(object sender, EventArgs e)
        {
            var adUser = CbuAd.Lib.User.GetSingle("mcrist");
        }

        private void cmdGetUserRoles_Click(object sender, EventArgs e)
        {
            var roles = new CbuAd.Lib.ActiveDirectoryRoleProvider().GetRolesForUser(txtUser.Text);

            StringBuilder sbRoles = new StringBuilder();

            foreach (var role in roles)
            {
                sbRoles.Append(role + Environment.NewLine);
            }

            MessageBox.Show(sbRoles.ToString());
        }

        private void cmdSemesterByCount_Click(object sender, EventArgs e)
        {
            var s = CbuCx.Lib.Academics.AcademicCalendarModel.GetSemesters(0,2);
        }

        private void cmdCurrentSemSubsession_Click(object sender, EventArgs e)
        {
            var s = CbuCx.Lib.Academics.AcademicCalendarModel.GetCurrentSemesterWithSubsession();
        }

        private void cmdFillAdmit_Click(object sender, EventArgs e)
        {
            RetentionAlertRepository r = new RetentionAlertRepository();

            var students = r.GetStudentInfos();

            foreach (var student in students)
            {
                try
                {
                    var cxInfo = CbuCx.Lib.User.StudentUser.GetStudentInfo(int.Parse(student.StudentId));

                    if (cxInfo != null)
                    {
                        //update the admit info
                        r.UpdateStudentAdmitInfo(cxInfo.AdmitSemester, cxInfo.AdmitYear, student.StudentId);
                    }
                }
                catch (Exception ex)
                {

                }
            }
        }

        private void cmdE2Start_Click(object sender, EventArgs e)
        {
            var s = CbuCx.Lib.Academics.AcademicCalendarModel.GetCurrentSemesterWithSubsession();

            if (s.SubSession == "E2")
            {

                bool e2Start = CbuCx.Lib.User.StudentUser.IsNewSecondSessionStudent(569541, "SP", 2013);
            }
        }

        private void cmdReport_Click(object sender, EventArgs e)
        {
            var data = CbuRetentionAlert.Lib.Models.ReportBySessionModel.GetReportData("SP", 2013);

            foreach (var item in data)
            {
                
            }
        }

    }
}
