﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CbuRetentionAlert.Web.Models
{
    public class CourseModel
    {
        public string CourseNumber { get; set; }
        public string Semester { get; set; }
        public string Subsession { get; set; }
        public int Year { get; set; }
        public int MaxEnrollment { get; set; }
        public int CurrentEnrollment { get; set; }
        public string Title { get; set; }
        public double Units { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public List<DayOfWeek> Days { get; set; }
        public string Building { get; set; }
        public string Room { get; set; }
        public string DeliveryMethod { get; set; }
    }
}