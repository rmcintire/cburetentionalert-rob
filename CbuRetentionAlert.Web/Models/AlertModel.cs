﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CbuRetentionAlert.Web.Models
{
    public class AlertModel
    {
        public StudentModel Student { get; set; }
        public InstructorModel Instructor { get; set; }
        public CourseModel Course { get; set; }

        public string Advisor { get; set; }
        public DateTime? LastDateAttended { get; set; }
        public string Concern { get; set; }
        public string GradeComments { get; set; }
        public bool? IsGradeSalvageable { get; set; }
        public bool? HasStudentRequestedHelp { get; set; }
        public bool? HasContactedStudent { get; set; }

        public DateTime Submitted { get; set; }
        public string SubmittedBy { get; set; }

        public string Status { get; set; }
        public DateTime? FollowUp { get; set; }

        public DateTime? IsNewStudentTillDate { get; set; }

        public List<ActionModel> Actions { get; set; }

        public int AlertId { get; set; }

        public AlertModel()
        {
            
        }
    }
}