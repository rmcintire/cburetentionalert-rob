﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CbuRetentionAlert.Web.Models
{
    public class AlertSubmittedResultsModel
    {
        public List<SubmitResult> Results { get; set; }
    }

    public class SubmitResult
    {
        public string StudentName { get; set; }
        public string StudentID { get; set; }
        public bool Successful { get; set; }
    }
}