﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CbuRetentionAlert.Web.Models
{
    public class StudentAlertModel
    {
        public StudentModel Student { get; set; }
        public List<AlertModel> Alerts { get; set; }
    }
}