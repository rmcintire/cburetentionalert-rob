﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CbuRetentionAlert.Web.Models
{
    public class ActionModel
    {
        public string ActionType { get; set; }
        public string Comment { get; set; }
        public DateTime? FollowUpDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime Created { get; set; }
    }
}