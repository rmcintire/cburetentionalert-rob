﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.IO;

namespace CbuRetentionAlert.Web.Models
{
    public class ReportRetentionAlertBySession : ActionResult
    {
        public ReportRetentionAlertBySession()
        {

        }

        public string Session { get; set; }
        public int Year { get; set; }

        public override void ExecuteResult(ControllerContext context)
        {
            try
            {
                string fileName = "Retention-Alerts-By-SemesterYear-" + DateTime.Now.ToString("MM-dd-yyyy-hh-mm-ss") + ".csv";

                //create csv file
                StringBuilder sbCSVFileData = new StringBuilder();

                //Columns Headers
                sbCSVFileData.Append("First Name,Last Name,Major,SubProgram,Admit Session,Admit Year,Course Number,Course Section,Semester,Year,Advisor,Status,Concern," +
                                     "Blackboard Administratively Generated,Submitted On,Responded On,Time To Response,Completed On,Time To Completion,Resolution" + Environment.NewLine);

                var data = CbuRetentionAlert.Lib.Models.ReportBySessionModel.GetReportData(Session, Year);

                foreach (var item in data)
                {
                    sbCSVFileData.Append(item.FirstName + ",");
                    sbCSVFileData.Append(item.LastName + ",");
                    sbCSVFileData.Append(item.Major + ",");
                    sbCSVFileData.Append(item.SubProgram + ",");
                    sbCSVFileData.Append(item.AdmitSemester + ",");
                    sbCSVFileData.Append(item.AdmitYear + ",");
                    sbCSVFileData.Append(item.CourseNumber + ",");
                    sbCSVFileData.Append(item.CourseSection + ",");
                    sbCSVFileData.Append(item.Semester + ",");
                    sbCSVFileData.Append(item.Year + ",");
                    sbCSVFileData.Append(item.Advisor + ",");
                    sbCSVFileData.Append(item.Status + ",");
                    sbCSVFileData.Append(item.Concern + ",");
                    sbCSVFileData.Append((item.IsBlackboardAdminGenerated.HasValue ? (item.IsBlackboardAdminGenerated.Value ? "Yes" : "No") : "No") + ",");
                    sbCSVFileData.Append(item.Created.ToString() + ",");
                    sbCSVFileData.Append((item.FirstRespondedOn.HasValue ? item.FirstRespondedOn.Value.ToString() : "") + ",");
                    sbCSVFileData.Append(item.TimeToResponse + ",");
                    sbCSVFileData.Append((item.CompletedOn.HasValue ? item.CompletedOn.Value.ToString() : "") + ",");
                    sbCSVFileData.Append(item.TimeToCompletion + ",");
                    sbCSVFileData.Append("\"" + (!string.IsNullOrEmpty(item.Resolution) ? item.Resolution.Replace("\"", "\"\"") : string.Empty) + "\"" + Environment.NewLine);

                }

                

                using (FileStream fs = File.Create(context.HttpContext.Request.PhysicalApplicationPath + "\\" + fileName))
                {
                    using (StreamWriter sw = new StreamWriter(fs))
                    {
                        sw.Write(sbCSVFileData.ToString());
                    }
                }

                context.HttpContext.Response.AddHeader("content-disposition",
                  "attachment; filename=" + fileName);
                context.HttpContext.Response.TransmitFile(context.HttpContext.Request.PhysicalApplicationPath + "\\" + fileName);
            }
            catch (Exception ex)
            {
                CbuUtilities.Utilities.LogException(ex);
            }
        }
    }
}