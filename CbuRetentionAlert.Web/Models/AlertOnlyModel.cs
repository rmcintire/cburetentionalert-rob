﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CbuRetentionAlert.Web.Models
{
    public class AlertOnlyModel
    {
        public StudentModel Student { get; set; }
        public InstructorModel Instructor { get; set; }
        public CourseModel Course { get; set; }
        public DateTime Submitted { get; set; }
        public string SubmittedBy { get; set; }
        public string Status { get; set; }
        public int Id { get; set; }
    }
}