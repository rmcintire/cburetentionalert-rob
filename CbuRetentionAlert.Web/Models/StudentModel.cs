﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CbuRetentionAlert.Web.Models
{
    public class StudentModel
    {
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string OtherPhone { get; set; }
        public string Major { get; set; }
        public string SubProgram { get; set; }
        public string AdmitSemester { get; set; }
        public string AdmitYear { get; set; }
        public int StudentId { get; set; }
        public int Id { get; set; }
    }
}