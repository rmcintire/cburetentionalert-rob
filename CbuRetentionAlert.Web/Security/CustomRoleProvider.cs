﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using CbuRetentionAlert.Lib.Repository;

namespace CbuRetentionAlert.Web.Security
{
    public class CustomRoleProvider : RoleProvider 
    {
        public override bool IsUserInRole(string username, string roleName)
        {
            bool isInRole = false;

            try
            {
                UserAdminRepository r = new UserAdminRepository();

                if (roleName == "Admin")
                {
                    isInRole = r.IsUserAdmin(username);
                }
            }
            catch(Exception ex)
            {
                CbuUtilities.Utilities.LogException(ex);
            }

            return isInRole;
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }

        public override string[] GetRolesForUser(string username)
        {
            List<string> roles = new List<string>();

            try
            {
                UserAdminRepository r = new UserAdminRepository();

                if (r.IsUserAdmin(username))
                {
                    roles.Add("Admin");
                }
            }
            catch(Exception ex)
            {
                CbuUtilities.Utilities.LogException(ex);
            }

            return roles.ToArray();
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }
    }
}