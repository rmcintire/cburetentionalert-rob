﻿$(document).ready(function () {


    // SCROLL TO FUNCTIONS
    $(".scrollTo").click(function () {
        var scrollPos = $("#scrollFrom" + $(this).attr("id")).offset().top - 16;

        $("html, body").animate({ scrollTop: scrollPos }, 1000, function () { BindScrollTop(); });

    });


    $(window).scroll(function () {
        if ($(window).scrollTop() <= 200) {
            $(".scrollTop").hide("fast");
            $(".scrollTop").remove();
        } else {
            BindScrollTop();
        }
    });

});


function showMask() {
    var maskHeight = $(document).height();
    var maskWidth = $(document).width();

    $('#mask').css({ 'width': maskWidth, 'height': maskHeight });
    $('#mask').fadeIn(100);
    $('#mask').fadeTo('fast', 0.5);
}
function showProcessing() {

    showMask();

    setTimeout("showLoader();", 10);
}

function showLoader() {
    var height = $(window).height();
    var width = $(window).width();

    //$('#spinner').css({ 'top': (height / 2) - ($('#spinner').height() / 2), 'left': (width / 2) - ($('#spinner').width() / 2) });
    $('#spinner').show();

}

function BindScrollTop() {

    $("body").append("<div class='scrollTop' style='display:none;'>^ Top</div>");
    $(".scrollTop").fadeIn("fast");

    $(".scrollTop").unbind("click");
    $(".scrollTop").click(function () {
        $("html, body").animate({ scrollTop: 0 }, 500);
        $(".scrollTop").fadeOut("fast");
        $(".scrollTop").remove();
    });

    $(".scrollTop:not(:first)").remove();

}