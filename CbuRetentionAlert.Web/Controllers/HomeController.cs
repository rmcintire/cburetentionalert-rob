﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CbuRetentionAlert.Web.Models;
using CbuRetentionAlert.Web.ViewModels;

namespace CbuStudentLookup.Web.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        [Authorize]
        public ActionResult Index()
        {
            HomeViewModel model = new HomeViewModel();

            if (User.IsInRole("Admin"))
            {
                List<SelectListItem> items = new List<SelectListItem>();
                CbuRetentionAlert.Lib.Models.RetentionAlertModel vm = new CbuRetentionAlert.Lib.Models.RetentionAlertModel();
                var semesters = vm.GetSemesters();

                string semester = Request.Form["AdminAlerts.semester"];

                if (!string.IsNullOrEmpty(semester))
                {
                    string[] sem = semester.Split('-');

                    if (sem.Length == 2)
                    {
                        int year = DateTime.Now.Year;

                        int.TryParse(sem[1], out year);

                        model.LoadAdminAlerts(sem[0], year);

                        foreach (var s in semesters)
                        {
                            if (s.Year == year && s.Semester == sem[0])
                            {
                                items.Add(new SelectListItem { Text = s.Semester + " " + s.Year, Value = s.Semester + "-" + s.Year, Selected = true });
                            }
                            else
                            {
                                items.Add(new SelectListItem { Text = s.Semester + " " + s.Year, Value = s.Semester + "-" + s.Year });
                            }
                        }

                        ViewBag.Semesters = items;
                    }
                    else
                    {
                        model.LoadAdminAlerts(semesters[0].Semester, semesters[0].Year);

                        foreach (var s in semesters)
                        {
                            items.Add(new SelectListItem { Text = s.Semester + " " + s.Year, Value = s.Semester + "-" + s.Year });
                        }

                        ViewBag.Semesters = items;
                    }                    
                }
                else
                {
                    foreach (var s in semesters)
                    {
                        items.Add(new SelectListItem { Text = s.Semester + " " + s.Year, Value = s.Semester + "-" + s.Year });
                    }

                    ViewBag.Semesters = items;

                    model.LoadAdminAlerts(semesters[0].Semester, semesters[0].Year);
                }
            }
            else
            {
                var adUser = CbuAd.Lib.User.GetSingle(User.Identity.Name.Replace("@calbaptist.local", ""));

                //var adUser = CbuAd.Lib.User.GetUser("sclow");

                if (adUser != null && !string.IsNullOrEmpty(adUser.EmployeeId))
                {
                    int employeeId = -1;

                    if (int.TryParse(adUser.EmployeeId, out employeeId))
                    {
                        model.LoadAlerts(employeeId);
                    }
                }
            }

            return View(model);
        }
    }
}
