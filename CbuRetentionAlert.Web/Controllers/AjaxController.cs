﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CbuRetentionAlert.Lib.Repository;

namespace CbuRetentionAlert.Web.Controllers
{
    public class AjaxController : Controller
    {
        //
        // GET: /Ajax/

        [Authorize]
        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        public void SaveStudentPhone(int studentId, string phone)
        {
            string response = string.Empty;

            try
            {
                RetentionAlertRepository r = new RetentionAlertRepository();

                if (r.UpdateStudentOtherPhone(studentId, phone))
                {
                    response = "success";
                }
                else
                {
                    response = "error";
                }
            }
            catch (Exception ex)
            {
                CbuUtilities.Utilities.LogException(ex);
            }
            finally
            {
                Response.Clear();
                Response.ContentType = ("text/html");
                Response.BufferOutput = true;
                Response.Write(response);
                Response.End();
            }
        }

        [Authorize]
        public void SaveAlertDetails(int alertId, int advisorId)
        {
            string response = string.Empty;

            try
            {
                RetentionAlertRepository r = new RetentionAlertRepository();

                if (r.SaveAlertDetails(alertId, advisorId))
                {
                    response = "success";
                }
                else
                {
                    response = "error";
                }
            }
            catch (Exception ex)
            {
                CbuUtilities.Utilities.LogException(ex);
            }
            finally
            {
                Response.Clear();
                Response.ContentType = ("text/html");
                Response.BufferOutput = true;
                Response.Write(response);
                Response.End();
            }
        }

        [Authorize]
        public void SaveAction(int alertId, int actionId, string comment, string followUpDate)
        {
            string response = string.Empty;

            try
            {
                RetentionAlertRepository r = new RetentionAlertRepository();

                if (!string.IsNullOrEmpty(followUpDate))
                {
                    DateTime _followUpdate;

                    if (DateTime.TryParse(followUpDate, out _followUpdate))
                    {
                        if (r.SaveAlertAction(alertId, actionId, comment, _followUpdate, User.Identity.Name))
                        {
                            response = "success";
                        }
                        else
                        {
                            response = "error";
                        }
                    }
                    else
                    {
                        response = "invalid followup date";
                    }
                }
                else
                {
                    if (r.SaveAlertAction(alertId, actionId, comment, null, User.Identity.Name))
                    {
                        response = "success";
                    }
                    else
                    {
                        response = "error";
                    }
                }

            }
            catch (Exception ex)
            {
                CbuUtilities.Utilities.LogException(ex);
            }
            finally
            {
                Response.Clear();
                Response.ContentType = ("text/html");
                Response.BufferOutput = true;
                Response.Write(response);
                Response.End();
            }
        }

        [Authorize]
        public void SaveInstructorAlertAction(int alertId, string action, string comment)
        {
            string response = string.Empty;

            try
            {
                RetentionAlertRepository r = new RetentionAlertRepository();

                if (r.SaveInstructorAlertAction(alertId, action, comment, User.Identity.Name))
                {
                    response = "success";
                }
                else
                {
                    response = "error";
                }

            }
            catch (Exception ex)
            {
                CbuUtilities.Utilities.LogException(ex);
            }
            finally
            {
                Response.Clear();
                Response.ContentType = ("text/html");
                Response.BufferOutput = true;
                Response.Write(response);
                Response.End();
            }
        }



    }
}
