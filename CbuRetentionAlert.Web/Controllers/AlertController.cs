﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CbuRetentionAlert.Web.Models;
using System.Text;
using CbuRetentionAlert.Web.ViewModels;

namespace CbuRetentionAlert.Web.Controllers
{
    public class AlertController : Controller
    {
        //
        // GET: /Alert/

        [Authorize]
        public ActionResult Index(int id)
        {
            if (User.IsInRole("Admin"))
            {
                AlertViewModel model = new AlertViewModel();
                model.LoadAlert(id);

                return View(model);
            }
            else
            {
                return RedirectToAction("index", "home");
            }
        }

        [Authorize]
        public ActionResult StudentAlert(int id)
        {
            
                AlertViewModel model = new AlertViewModel();
                model.LoadAlert(id);

                return View(model);
        }

        [Authorize]
        [HttpGet]
        public ActionResult New()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult New(string txtSFirstName, string txtSLastName, string txtSID, string txtFFirstName, string txtFLastName, string txtFEmail, string txtFID,
                                string txtCourse, string txtSection, string ddlSemester, int ddlYear, string txtAttendDate, string ddlConcern, string grade,
                                string comments, string studentContact, string instructorContact)
        {
            int studentId = -1;
            DateTime lastAttended = DateTime.Now;

            if (int.TryParse(txtSID, out studentId) && DateTime.TryParse(txtAttendDate, out lastAttended))
            {
                var retentionAlert = new Lib.Models.RetentionAlertModel();

                int facultyId = -1;

                int.TryParse(txtFID, out facultyId);

                if (retentionAlert.SaveRetentionAlert(studentId, txtFFirstName, string.Empty, txtFLastName, txtFEmail, facultyId, txtCourse, txtSection, ddlSemester, ddlYear,
                                                  lastAttended, ddlConcern, (grade == "Yes" ? true : false), comments, (studentContact == "Yes" ? true : false), (instructorContact == "Yes" ? true : false), User.Identity.Name.Replace("@calbaptist.local", ""), null))
                {
                    AlertSubmittedResultsModel model = new AlertSubmittedResultsModel();

                    model.Results = new List<SubmitResult> { new SubmitResult { StudentID = txtSID, StudentName = txtSFirstName + " " + txtSLastName, Successful = true } };

                    TempData["Results"] = model;

                    return RedirectToAction("alertsubmitted");
                }
                else
                {
                    ViewData["ErrorMessage"] = "There was an error while trying to save this retention alert, please double check the Student ID and then try again.";
                    return View();
                }
            }
            else
            {
                ViewData["ErrorMessage"] = "There was an error while trying to save this retention alert, please double check the Student ID and then try again.";
                return View();
            }
        }

        [Authorize]
        public ActionResult NewStep1()
        {
            NewAlertViewModel model = new NewAlertViewModel();

            var adUser = CbuAd.Lib.User.GetSingle(User.Identity.Name.Replace("@calbaptist.local", ""));

            if (adUser != null && !string.IsNullOrEmpty(adUser.EmployeeId))
            {
                int employeeId = -1;

                if (int.TryParse(adUser.EmployeeId, out employeeId))
                {
                    model.LoadInstructorCourses(employeeId);
                }
            }

            //model.LoadInstructorCourses(212325);

            return View(model);
        }

        [Authorize]
        [HttpGet]
        public ActionResult NewStep2(string c, string p)
        {
            NewAlertViewModel model = new NewAlertViewModel();

            model.LoadStudentEnrollments(c);

            if (!string.IsNullOrEmpty(p))
            {
                model.PreviousStep = p;
            }

            return View(model);
        }

        [Authorize]
        [HttpPost]
        public ActionResult NewStep3(string p)
        {
            string course = Request["course"];
            var studentIds = new List<string>();

            for (int i = 0; i < Request.Form.Keys.Count; i++)
            {
                if (Request.Form.Keys[i].StartsWith("chkId_"))
                {
                    studentIds.Add(Request[Request.Form.Keys[i]]);
                }
            }

            NewAlertViewModel model = new NewAlertViewModel();

            model.LoadRetentionAlertToBeSubmitted(studentIds, course);

            if (!string.IsNullOrEmpty(p))
            {
                model.PreviousStep = p;
            }

            return View(model);
        }

        [Authorize]
        [HttpGet]
        public ActionResult NewByCourse()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult NewByCourse(string txtCourse, string txtSection, string ddlSemester, string ddlYear)
        {
            return Redirect("~/alert/newstep2/?c=" + ddlYear + "-" + ddlSemester + "-" + txtCourse.ToUpper() + "-" + txtSection.ToUpper() + "&p=newbycourse");
        }

        [Authorize]
        [HttpGet]
        public ActionResult AlertSubmitted()
        {
            AlertSubmittedResultsModel model = new AlertSubmittedResultsModel();

            if (TempData["Results"] != null)
            {
                model = TempData["Results"] as AlertSubmittedResultsModel;
            }

            return View(model);
        }

        [Authorize]
        [HttpPost]
        public ActionResult AlertSubmitted(string course)
        {


            //got get all the posted data
            List<SubmitResult> results = new List<SubmitResult>();

            //get all the student Ids first
            List<string> studentIds = new List<string>();

            for (int i = 0; i < Request.Form.Keys.Count; i++)
            {
                if (Request.Form.Keys[i].StartsWith("studentId_"))
                {
                    studentIds.Add(Request[Request.Form.Keys[i]]);
                }
            }

            foreach (string studentId in studentIds)
            {
                //get each set of form values
                int studentIdVal = -1;
                DateTime lastAttended = DateTime.Now;

                if (int.TryParse(Request["studentId_" + studentId], out studentIdVal))
                {
                    var retentionAlert = new Lib.Models.RetentionAlertModel();

                    string facultyFirstName = Request["facultyFirstName_" + studentId];
                    string facultyMiddleName = Request["facultyMiddleName_" + studentId];
                    string facultyLastName = Request["facultyLastName_" + studentId];
                    string facultyEmail = Request["facultyEmailName_" + studentId];
                    string studentName = Request["studentName_" + studentId];

                    string concern = Request["concern_" + studentId];
                    string grade = Request["grade_" + studentId];
                    string comments = Request["comments_" + studentId];
                    string studentContact = Request["studentContact_" + studentId];
                    string instructorContact = Request["instructorContact_" + studentId];
                    string instructorId = Request["facultyId_" + studentId];

                    string bbGeneration = Request["chkBB_" + studentId];

                    int facultyId = -1;

                    int.TryParse(instructorId, out facultyId);

                    //string course = Request["course_" + studentId];
                    string courseNumber = string.Empty;
                    string sectionNumber = string.Empty;
                    string semester = string.Empty;
                    int year = -1;

                    string[] courseChunks = course.Split('-');

                    bool hasLastAttendedDate = false;

                    hasLastAttendedDate = DateTime.TryParse(Request["txtAttendDate_" + studentId], out lastAttended);

                    if (courseChunks.Length == 4)
                    {
                        courseNumber = courseChunks[2];
                        sectionNumber = courseChunks[3];
                        semester = courseChunks[1];

                        if (int.TryParse(courseChunks[0], out year))
                        {
                            if (retentionAlert.SaveRetentionAlert(studentIdVal, facultyFirstName, facultyMiddleName, facultyLastName, facultyEmail, facultyId, courseNumber, sectionNumber, semester, year,
                                (hasLastAttendedDate ? lastAttended : (DateTime?)null), concern, (!string.IsNullOrEmpty(grade) ? (grade == "Yes" ? true : false) : (bool?)null), comments,
                                (!string.IsNullOrEmpty(studentContact) ? (studentContact == "Yes" ? true : false) : (bool?)null),
                                (!string.IsNullOrEmpty(instructorContact) ? (instructorContact == "Yes" ? true : false) : (bool?)null), User.Identity.Name.Replace("@calbaptist.local", ""),
                                (!string.IsNullOrEmpty(bbGeneration) ? (bool?)true : null)))
                            {
                                results.Add(new SubmitResult { StudentID = studentId, StudentName = studentName, Successful = true });
                            }
                            else
                            {
                                results.Add(new SubmitResult { StudentID = studentId, StudentName = studentName, Successful = false });
                            }
                        }
                    }
                    else
                    {
                        results.Add(new SubmitResult { StudentID = studentId, StudentName = studentName, Successful = false });
                    }
                }
                else
                {
                    results.Add(new SubmitResult { StudentID = studentId, StudentName = string.Empty, Successful = false });
                }
            }

            AlertSubmittedResultsModel model = new AlertSubmittedResultsModel();
            model.Results = results;

            return View(model);
        }
    }
}
