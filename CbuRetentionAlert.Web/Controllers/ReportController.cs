﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CbuRetentionAlert.Web.Models;

namespace CbuRetentionAlert.Web.Controllers
{
    public class ReportController : Controller
    {
        //
        // GET: /Report/

        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        [HttpGet]
        public ActionResult Session()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult Session(string session, int? year)
        {
            if (!string.IsNullOrEmpty(session) && year.HasValue)
            {

                return new ReportRetentionAlertBySession { Session = session, Year = year.Value };
            }
            else
            {
                ViewData["ErrorMessage"] = "Invalid Session and Year, please try again.";
                return View();
            }
        }
    }
}
