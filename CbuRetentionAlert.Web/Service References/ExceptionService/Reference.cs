﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.17929
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CbuRetentionAlert.Web.ExceptionService {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="ExceptionService.ILogException")]
    public interface ILogException {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ILogException/Log", ReplyAction="http://tempuri.org/ILogException/LogResponse")]
        string Log(string exceptionMessage, string exceptionType, string applicationName, bool sendEmail, string emailTo, string emailSubject);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface ILogExceptionChannel : CbuRetentionAlert.Web.ExceptionService.ILogException, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class LogExceptionClient : System.ServiceModel.ClientBase<CbuRetentionAlert.Web.ExceptionService.ILogException>, CbuRetentionAlert.Web.ExceptionService.ILogException {
        
        public LogExceptionClient() {
        }
        
        public LogExceptionClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public LogExceptionClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public LogExceptionClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public LogExceptionClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public string Log(string exceptionMessage, string exceptionType, string applicationName, bool sendEmail, string emailTo, string emailSubject) {
            return base.Channel.Log(exceptionMessage, exceptionType, applicationName, sendEmail, emailTo, emailSubject);
        }
    }
}
