﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CbuRetentionAlert.Web.Models;

namespace CbuRetentionAlert.Web.ViewModels
{
    public class RetentionAlertToBeSubmittedViewModel
    {
        public List<StudentModel> Students { get; set; }
        public CourseModel Course { get; set; }
        public InstructorModel Instructor { get; set; }

        public static RetentionAlertToBeSubmittedViewModel GetModel(List<string> studentIds, string course)
        {
            var retentionAlert = new RetentionAlertToBeSubmittedViewModel();

            retentionAlert.Students = new List<StudentModel>();
            retentionAlert.Course = new CourseModel();
            retentionAlert.Instructor = new InstructorModel();

            try
            {
                if (!string.IsNullOrEmpty(course))
                {
                    string courseNumber = string.Empty;
                    string sectionNumber = string.Empty;
                    string semester = string.Empty;
                    int year = -1;

                    string[] courseChunks = course.Split('-');

                    if (courseChunks.Length == 4)
                    {
                        courseNumber = courseChunks[2];
                        sectionNumber = courseChunks[3];
                        semester = courseChunks[1];

                        if (int.TryParse(courseChunks[0], out year))
                        {
                            retentionAlert.Course.CourseNumber = courseNumber + "-" + sectionNumber;
                            retentionAlert.Course.Semester = semester;
                            retentionAlert.Course.Year = year;

                            var studentEnrollments = CbuCx.Lib.Academics.CourseEnrollmentModel.GetStudentEnrollmentsForCourse(courseNumber, sectionNumber, semester, year);

                            foreach (var enrollment in studentEnrollments)
                            {
                                if (studentIds.Contains(enrollment.StudentId.ToString()))
                                {
                                    retentionAlert.Students.Add(new StudentModel { FirstName = enrollment.FirstName, LastName = enrollment.LastName, MiddleName = enrollment.MiddleName, StudentId = enrollment.StudentId });
                                }
                            }

                            var cxCourse = CbuCx.Lib.Academics.CourseModel.GetCourse(courseNumber, sectionNumber, semester, year);
                            var instructor = CbuCx.Lib.User.FacultyUser.GetFacultyInfo(cxCourse.InstructorId);

                            retentionAlert.Instructor.EmailAddress = instructor.EmailAddress;
                            retentionAlert.Instructor.FirstName = instructor.FirstName;
                            retentionAlert.Instructor.InstructorId = instructor.FacultyId;
                            retentionAlert.Instructor.LastName = instructor.LastName;
                            retentionAlert.Instructor.MiddleName = instructor.MiddleName;
                            retentionAlert.Instructor.Suffix = instructor.Suffix;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CbuUtilities.Utilities.LogException(ex);
            }

            return retentionAlert;
        }
    }
}