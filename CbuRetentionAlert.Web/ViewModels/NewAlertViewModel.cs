﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CbuRetentionAlert.Web.Models;

namespace CbuRetentionAlert.Web.ViewModels
{
    public class NewAlertViewModel
    {
        public List<CourseModel> Courses { get; set; }
        public List<StudentModel> CourseStudents { get; set; }
        public RetentionAlertToBeSubmittedViewModel NewRetentionAlert { get; set; }
        public string CurrentCourse { get; set; }
        public string PreviousStep { get; set; }

        public NewAlertViewModel()
        {
            Courses = new List<CourseModel>();
            CourseStudents = new List<StudentModel>();
            NewRetentionAlert = new RetentionAlertToBeSubmittedViewModel();
            CurrentCourse = string.Empty;
        }

        public void LoadInstructorCourses(int facultyId)
        {
            Courses = new List<CourseModel>();

            try
            {
                var courses = CbuCx.Lib.Academics.CourseEnrollmentModel.GetFacultyCoursesForTheCurrentSemester(facultyId);

                foreach (var course in courses)
                {
                    var courseModel = new CourseModel
                    {
                        Building = course.Building + course.Building2,
                        CourseNumber = course.FullCourseNumber,
                        Days = course.DaysOfTheWeek,
                        DeliveryMethod = course.Deilvery.ToString(),
                        Room = course.Room + course.Room2,
                        Semester = course.Semester,
                        Subsession = course.SubSession,
                        Title = course.Title,
                        Year = course.Year
                    };


                    courseModel.CurrentEnrollment = (course.StudentEnrollment.HasValue ? course.StudentEnrollment.Value : 0);
                    courseModel.EndDate = (course.EndDate.HasValue ? course.EndDate.Value.ToString("MM/dd/yyyy") : string.Empty);
                    courseModel.EndTime = (course.EndTime.HasValue ? course.EndTime.Value.ToString() : string.Empty);
                    courseModel.MaxEnrollment = (course.MaxEnrollment.HasValue ? course.MaxEnrollment.Value : 0);
                    courseModel.StartDate = (course.BeginDate.HasValue ? course.BeginDate.Value.ToString("MM/dd/yyyy") : string.Empty);
                    courseModel.StartTime = (course.BeginTime.HasValue ? course.BeginTime.Value.ToString() : string.Empty);
                    courseModel.Units = (course.Units.HasValue ? course.Units.Value : 0);

                    Courses.Add(courseModel);

                }
            }
            catch (Exception ex)
            {
                CbuUtilities.Utilities.LogException(ex);
            }
        }

        public void LoadStudentEnrollments(string course)
        {
            CourseStudents = new List<StudentModel>();
            CurrentCourse = course;

            try
            {
                if (!string.IsNullOrEmpty(course))
                {
                    string courseNumber = string.Empty;
                    string sectionNumber = string.Empty;
                    string semester = string.Empty;
                    int year = -1;

                    string[] courseChunks = course.Split('-');

                    if (courseChunks.Length == 4)
                    {
                        courseNumber = courseChunks[2];
                        sectionNumber = courseChunks[3];
                        semester = courseChunks[1];

                        if (int.TryParse(courseChunks[0], out year))
                        {
                            var studentEnrollments = CbuCx.Lib.Academics.CourseEnrollmentModel.GetStudentEnrollmentsForCourse(courseNumber, sectionNumber, semester, year);

                            foreach (var enrollment in studentEnrollments)
                            {
                                CourseStudents.Add(new StudentModel { FirstName = enrollment.FirstName, LastName = enrollment.LastName, MiddleName = enrollment.MiddleName, StudentId = enrollment.StudentId });
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CbuUtilities.Utilities.LogException(ex);
            }
        }

        public void LoadRetentionAlertToBeSubmitted(List<string> studentIds, string course)
        {
            CurrentCourse = course;
            NewRetentionAlert = RetentionAlertToBeSubmittedViewModel.GetModel(studentIds, course);
        }
    }
}