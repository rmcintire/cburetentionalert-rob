﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CbuRetentionAlert.Web.Models;
using CbuRetentionAlert.Lib.Repository;
using CbuRetentionAlert.Lib.Models;

namespace CbuRetentionAlert.Web.ViewModels
{
    public class AlertViewModel
    {
        public AlertModel Alert { get; set; }
        public List<AdvisorModel> Advisors { get; set; }
        public List<StatusLookupModel> Statuses { get; set; }
        public List<ActionLookupModel> Actions { get; set; }

        public AlertViewModel()
        {
            Alert = new AlertModel();
            Advisors = AdvisorLookupRepository.GetAdvisors();
            Statuses = StatusLookupRepository.GetStatuses();
            Actions = ActionLookupRepository.GetActions();
        }

        public void LoadAlert(int alertId)
        {
            try
            {
                RetentionAlertRepository r = new RetentionAlertRepository();

                var alert = r.GetRetentionAlertById(alertId);

                if (alert != null)
                {
                    var actions = new List<ActionModel>();

                    if (alert.Actions != null)
                    {
                        foreach (var action in alert.Actions)
                        {
                            actions.Add(new ActionModel
                            {
                                ActionType = action.ActionType,
                                Comment = action.Comment,
                                Created = action.Created,
                                CreatedBy = action.CreatedBy,
                                FollowUpDate = action.FollowUpDate
                            });
                        }
                    }

                    Alert.Actions = actions;
                    Alert.Advisor = alert.Alert.Advisor;
                    Alert.AlertId = alert.Alert.Id;
                    Alert.Concern = alert.Alert.Concern;
                    Alert.Course = new CourseModel
                    {
                        CourseNumber = alert.Alert.CourseNumber + "-" + alert.Alert.CourseSection,
                        Semester = alert.Alert.Semester,
                        Year = alert.Alert.Year
                    };
                    Alert.FollowUp = null;
                    Alert.GradeComments = alert.Alert.GradeComments;
                    Alert.HasContactedStudent = alert.Alert.HasContactedStudent;
                    Alert.HasStudentRequestedHelp = alert.Alert.HasStudentRequestedHelp;
                    Alert.Instructor = new InstructorModel
                    {
                        EmailAddress = alert.Alert.InstructorEmail,
                        FirstName = alert.Alert.InstructorFirstName,
                        InstructorId = alert.Alert.InstructorId,
                        LastName = alert.Alert.InstructorLastName,
                        MiddleName = alert.Alert.InstructorMiddleName
                    };
                    Alert.IsGradeSalvageable = alert.Alert.IsGradeSalvageable;
                    Alert.LastDateAttended = alert.Alert.LastDateAttended;
                    Alert.Status = alert.Alert.Status;
                    Alert.Student = new StudentModel
                    {
                        Email = alert.StudentInfo.EmailAddress,
                        FirstName = alert.StudentInfo.FirstName,
                        LastName = alert.StudentInfo.LastName,
                        Major = alert.StudentInfo.Major,
                        MiddleName = alert.StudentInfo.MiddleName,
                        OtherPhone = alert.StudentInfo.OtherPhone,
                        Phone = alert.StudentInfo.PhoneNumber,
                        SubProgram = alert.StudentInfo.SubProgram,
                        Id = alert.StudentInfo.Id
                        
                    };

                    int studentId = -1;
                    int.TryParse(alert.StudentInfo.StudentId, out studentId);

                    Alert.Student.StudentId = studentId;

                    Alert.Submitted = alert.Alert.Created;
                    Alert.SubmittedBy = alert.Alert.CreatedBy;
                }
            }
            catch (Exception ex)
            {
                CbuUtilities.Utilities.LogException(ex);
            }
        }

        public bool IsAutoCompleteAlert(string subProgram)
        {
            bool isAutoComplete = false;

            switch (subProgram)
            {
                case "TRAD":
                case "MMAN":
                case "ESLG":
                case "ESLU":
                case "VABR":
                    isAutoComplete = true;
                    break;
            }

            return isAutoComplete;
        }
    }
}