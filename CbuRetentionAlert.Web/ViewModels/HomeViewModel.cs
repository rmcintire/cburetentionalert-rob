﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CbuRetentionAlert.Web.Models;
using System.Web.Mvc;

namespace CbuRetentionAlert.Web.ViewModels
{
    public class HomeViewModel
    {
        public List<AlertOnlyModel> Alerts { get; set; }
        public List<StudentAlertModel> AdminAlerts { get; set; }

        public void LoadAlerts(int cxId)
        {
            try
            {

                CbuRetentionAlert.Lib.Models.RetentionAlertModel model = new Lib.Models.RetentionAlertModel();

                var alerts = model.GetRetentionAlertsByInstructorId(cxId);

                foreach (var alert in alerts)
                {
                    Alerts.Add(new AlertOnlyModel
                    {
                        Course = new CourseModel { CourseNumber = alert.Alert.Year + "-" + alert.Alert.Semester + " " + alert.Alert.CourseNumber + "-" + alert.Alert.CourseSection },
                        Instructor = new InstructorModel { FirstName = alert.Alert.InstructorFirstName, LastName = alert.Alert.InstructorLastName },
                        Student = new StudentModel { FirstName = alert.StudentInfo.FirstName, LastName = alert.StudentInfo.LastName },
                        Submitted = alert.Alert.Created,
                        SubmittedBy = alert.Alert.CreatedBy,
                        Status = alert.Alert.Status,
                        Id = alert.Alert.Id
                    });
                }

                Alerts = Alerts.OrderByDescending(x => x.Submitted).ToList();
            }
            catch (Exception ex)
            {
                CbuUtilities.Utilities.LogException(ex);
            }
        }

        public void LoadAdminAlerts(string semester, int year)
        {
            try
            {

                CbuRetentionAlert.Lib.Models.RetentionAlertModel model = new Lib.Models.RetentionAlertModel();

                var students = new List<Lib.Models.StudentInfo>();

                students = model.GetAllRetentionAlerts(semester, year);

                foreach (var student in students)
                {
                    var alerts = new List<AlertModel>();

                    foreach (var alert in student.Alerts)
                    {
                        var _alert = new AlertModel
                        {
                            Course = new CourseModel { CourseNumber = alert.Alert.CourseNumber + "-" + alert.Alert.CourseSection, Semester = alert.Alert.Semester, Year = alert.Alert.Year },
                            Instructor = new InstructorModel { FirstName = alert.Alert.InstructorFirstName, LastName = alert.Alert.InstructorLastName, InstructorId = alert.Alert.InstructorId, EmailAddress = alert.Alert.InstructorEmail },
                            Status = alert.Alert.Status,
                            Submitted = alert.Alert.Created,
                            SubmittedBy = alert.Alert.CreatedBy,
                            Advisor = alert.Alert.Advisor,
                            AlertId = alert.Alert.Id,
                            IsNewStudentTillDate = alert.Alert.IsNewStudentTillDate
                        };


                        _alert.FollowUp = alert.Alert.FollowUpDate;

                        alerts.Add(_alert);
                    }

                    var studentModel = new StudentModel
                    {
                        FirstName = student.Student.FirstName,
                        LastName = student.Student.LastName,
                        MiddleName = student.Student.MiddleName,
                        Email = student.Student.EmailAddress,
                        Phone = student.Student.PhoneNumber,
                        AdmitSemester = student.Student.AdmitSemester,
                        AdmitYear = (student.Student.AdmitYear.HasValue ? student.Student.AdmitYear.Value.ToString() : string.Empty)
                    };

                    int studentId = 0;

                    int.TryParse(student.Student.StudentId, out studentId);

                    studentModel.StudentId = studentId;

                    AdminAlerts.Add(new StudentAlertModel { Alerts = alerts, Student = studentModel });
                }


                
            }
            catch (Exception ex)
            {
                CbuUtilities.Utilities.LogException(ex);
            }
        }

        public HomeViewModel()
        {
            Alerts = new List<AlertOnlyModel>();
            AdminAlerts = new List<StudentAlertModel>();
        }
    }
}