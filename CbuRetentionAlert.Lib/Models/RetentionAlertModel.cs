﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CbuRetentionAlert.Lib.Repository;
using CbuRetentionAlert.Lib.Enum;
using System.Configuration;
using CbuVelocify.Lib.Repository;
using CbuCx.Lib.User;

namespace CbuRetentionAlert.Lib.Models
{
    public class RetentionAlertModel
    {
        EmailService.EmailAddress[] StringToEmailArray(string to)
        {
            var toList = to.Split(',').Select(x => x.Trim()).ToList();

            if (toList.Count > 1 && toList.Any(x => !x.Contains('@')))
            {
                //at least one of the address does not have an @ so just treat the string as one "to" address, this is for safety/legacy support of addresses with commas in them for other reasons
                toList = new List<string>() { to };
            }

            var emailAddressList = new List<EmailService.EmailAddress>();
            foreach (string address in toList)
                emailAddressList.Add(new EmailService.EmailAddress { Address = address });

            return emailAddressList.ToArray();
        }

        public bool SaveRetentionAlert(int studentId, string facultyFirstName, string facultyMiddleName, string facultyLastName, string facultyEmail, int facultyId,
                                string course, string section, string semester, int year, DateTime? lastDateAttended, string concern, bool? isGradeSalvageable,
                                string gradeComments, bool? hasStudentRequestedHelp, bool? hasContactedStudent, string submittedBy, bool? bbGenerated)
        {
            bool saved = false;

            try
            {
                var cxStudent = CbuCx.Lib.User.StudentUser.GetStudentInfo(studentId);

                if (cxStudent.CxStudentId > -1)
                {
                    bool isAutoComplete = IsAutoCompleteAlert(cxStudent.SubProgram);

                    var tblStudent = new tblStudentInfo();

                    tblStudent.Created = DateTime.Now;
                    tblStudent.EmailAddress = cxStudent.EmailAddress;
                    tblStudent.FirstName = cxStudent.FirstName;
                    tblStudent.LastName = cxStudent.LastName;
                    tblStudent.Major = cxStudent.Major;
                    tblStudent.MiddleName = cxStudent.MiddleName;
                    tblStudent.PhoneNumber = cxStudent.PhoneNumber;
                    tblStudent.Prefix = string.Empty;
                    tblStudent.StudentId = cxStudent.CxStudentId.ToString();
                    tblStudent.SubProgram = cxStudent.SubProgram;
                    tblStudent.Suffix = cxStudent.Suffix;
                    tblStudent.OtherPhone = string.Empty;
                    tblStudent.AdmitSemester = cxStudent.AdmitSemester;
                    tblStudent.AdmitYear = cxStudent.AdmitYear;

                    var tblStudentAlert = new tblStudentAlert();

                    tblStudentAlert.Advisor = GetAdvisor(cxStudent.CxStudentId);
                    tblStudentAlert.Concern = concern;
                    tblStudentAlert.CourseNumber = course;
                    tblStudentAlert.CourseSection = section;
                    tblStudentAlert.Created = DateTime.Now;
                    tblStudentAlert.GradeComments = gradeComments;
                    tblStudentAlert.HasContactedStudent = hasContactedStudent;
                    tblStudentAlert.HasStudentRequestedHelp = hasStudentRequestedHelp;
                    tblStudentAlert.InstructorEmail = facultyEmail;
                    tblStudentAlert.InstructorFirstName = facultyFirstName;
                    tblStudentAlert.InstructorLastName = facultyLastName;
                    tblStudentAlert.InstructorMiddleName = facultyMiddleName;
                    tblStudentAlert.IsGradeSalvageable = isGradeSalvageable;
                    tblStudentAlert.LastDateAttended = lastDateAttended;
                    tblStudentAlert.Semester = semester;
                    tblStudentAlert.SubSession = string.Empty;
                    tblStudentAlert.Year = year;
                    tblStudentAlert.CreatedBy = submittedBy;
                    tblStudentAlert.InstructorId = facultyId;



                    tblStudentAlert.FollowUpDate = null;
                    tblStudentAlert.IsBlackboardAdminGenerated = (bbGenerated.HasValue ? (bool?)bbGenerated.Value : null);

                    //check here to see if this student needs to be assigned to admissions if it is not an auto complete
                    if (!isAutoComplete)
                    {
                        tblStudentAlert.IsNewStudentTillDate = GetIsNewStudentTillDate(cxStudent.AdmitSemester, cxStudent.AdmitYear, cxStudent.CxStudentId);
                    }

                    var tblStudentAlertAction = new tblStudentAlertAction();

                    if (isAutoComplete)
                    {
                        tblStudentAlert.Status = "Completed";
                        tblStudentAlert.AlertPriority = (int)AlertPriorityEnum.Completed;

                        tblStudentAlertAction.ActionType = "Complete";
                        tblStudentAlertAction.Comment = "Main Campus Student; Auto Completed.";
                        tblStudentAlertAction.Created = DateTime.Now;
                        tblStudentAlertAction.CreatedBy = "System";
                    }
                    else
                    {
                        //this alert needs to be managed by admissions
                        if (tblStudentAlert.IsNewStudentTillDate.HasValue)
                        {
                            tblStudentAlert.Status = "New";
                            tblStudentAlert.AlertPriority = (int)AlertPriorityEnum.New;

                            tblStudentAlertAction.ActionType = "Comment";
                            tblStudentAlertAction.Comment = "New alert submitted by " + submittedBy + ".";
                            tblStudentAlertAction.Created = DateTime.Now;
                            tblStudentAlertAction.CreatedBy = submittedBy;
                        }
                        else //this alert will be completed and managed in the LEARN Advising Application
                        {
                            tblStudentAlert.Status = "Completed";
                            tblStudentAlert.AlertPriority = (int)AlertPriorityEnum.Completed;

                            tblStudentAlertAction.ActionType = "Complete";
                            tblStudentAlertAction.Comment = "New alert submitted by " + submittedBy + ".  Case created in the LEARN application for Advising; Auto Completed.";
                            tblStudentAlertAction.Created = DateTime.Now;
                            tblStudentAlertAction.CreatedBy = submittedBy;
                        }
                    }

                    RetentionAlertRepository r = new RetentionAlertRepository();
                    saved = r.SaveRetentionAlert(tblStudent, tblStudentAlert, tblStudentAlertAction);

                    if (saved)
                    {
                        //queue the email notification here
                        EmailService.EmailServiceClient client = new EmailService.EmailServiceClient();

                        StringBuilder sbEmailBody = new StringBuilder();

                        sbEmailBody.Append("<table cellpadding=\"4\" cellspacing=\"2\" style=\"font-family:arial;font-size:12pt;\">");
                        sbEmailBody.Append("<tr><td align=\"right\" style=\"font-family:arial;font-size:14pt;\"><strong>*** Retention Alert ***</strong></td><td></td></tr>");
                        sbEmailBody.Append("<tr><td colspan=\"2\">&nbsp;</td></tr>");
                        sbEmailBody.Append("<tr><td align=\"right\" style=\"font-family:arial;font-size:14pt;\"><strong>Student Information</strong></td><td></td></tr>");
                        sbEmailBody.Append("<tr><td align=\"right\"><strong>Student First Name:</strong></td><td>" + cxStudent.FirstName + "</td></tr>");
                        sbEmailBody.Append("<tr><td align=\"right\"><strong>Student Middle Name:</strong></td><td>" + cxStudent.MiddleName + "</td></tr>");
                        sbEmailBody.Append("<tr><td align=\"right\"><strong>Student Last Name:</strong></td><td>" + cxStudent.LastName + "</td></tr>");
                        sbEmailBody.Append("<tr><td align=\"right\"><strong>Student ID:</strong></td><td>" + cxStudent.CxStudentId + "</td></tr>");
                        sbEmailBody.Append("<tr><td align=\"right\"><strong>Student Phone:</strong></td><td>" + cxStudent.PhoneNumber + "</td></tr>");
                        sbEmailBody.Append("<tr><td align=\"right\"><strong>Student Email:</strong></td><td><a href=\"mailto:" + cxStudent.EmailAddress + "\" >" + cxStudent.EmailAddress + "</a></td></tr>");
                        sbEmailBody.Append("<tr><td align=\"right\"><strong>Major:</strong></td><td>" + cxStudent.Major + "</td></tr>");
                        sbEmailBody.Append("<tr><td align=\"right\"><strong>Subprogram:</strong></td><td>" + cxStudent.SubProgram + "</td></tr>");
                        sbEmailBody.Append("<tr><td colspan=\"2\" align=\"center\">&nbsp;</td></tr>");
                        sbEmailBody.Append("<tr><td align=\"right\" style=\"font-family:arial;font-size:14pt;\"><strong>Instructor Information</strong></td><td></td></tr>");
                        sbEmailBody.Append("<tr><td align=\"right\"><strong>Instructor First Name:</strong></td><td>" + facultyFirstName + "</td></tr>");
                        sbEmailBody.Append("<tr><td align=\"right\"><strong>Instructor Middle Name:</strong></td><td>" + facultyMiddleName + "</td></tr>");
                        sbEmailBody.Append("<tr><td align=\"right\"><strong>Instructor Last Name:</strong></td><td>" + facultyLastName + "</td></tr>");
                        sbEmailBody.Append("<tr><td align=\"right\"><strong>Instructor Email:</strong></td><td><a href=\"mailto:" + facultyEmail + "\" >" + facultyEmail + "</a></td></tr>");
                        sbEmailBody.Append("<tr><td colspan=\"2\">&nbsp;</td></tr>");

                        sbEmailBody.Append("<tr><td align=\"right\" style=\"font-family:arial;font-size:14pt;\"><strong>Course Information</strong></td><td></td></tr>");
                        sbEmailBody.Append("<tr><td align=\"right\"><strong>Course Number:</strong> </td><td>" + course + "</td></tr>");
                        sbEmailBody.Append("<tr><td align=\"right\"><strong>Course Section:</strong></td><td>" + section + "</td></tr>");
                        sbEmailBody.Append("<tr><td align=\"right\"><strong>Semester:</strong></td><td>" + semester + "</td></tr>");
                        sbEmailBody.Append("<tr><td align=\"right\"><strong>Year:</strong></td><td>" + year + "</td></tr>");
                        sbEmailBody.Append("<tr><td colspan=\"2\">&nbsp;</td></tr>");

                        sbEmailBody.Append("<tr><td align=\"right\" style=\"font-family:arial;font-size:14pt;\"><strong>Retention Alert Information</strong></td><td></td></tr>");
                        sbEmailBody.Append("<tr><td align=\"right\"><strong>Assigned Advisor:</strong></td><td>" + tblStudentAlert.Advisor + "</td></tr>");
                        sbEmailBody.Append("<tr><td align=\"right\"><strong>Last Date Attended:</strong></td><td>" + (tblStudentAlert.LastDateAttended.HasValue ? tblStudentAlert.LastDateAttended.Value.ToString("MM/dd/yyyy hh:mm:ss tt") : "N/A") + "</td></tr>");
                        sbEmailBody.Append("<tr><td align=\"right\"><strong>Concern about student:</strong></td><td>" + concern + "</td></tr>");
                        sbEmailBody.Append("<tr><td align=\"right\"><strong>Is grade salvageable?</strong></td><td>" + (isGradeSalvageable.HasValue ? (isGradeSalvageable.Value ? "Yes" : "No") : "N/A") + "</td></tr>");
                        sbEmailBody.Append("<tr><td align=\"right\"><strong>Comments:</strong></td><td>" + gradeComments + "</td></tr>");
                        sbEmailBody.Append("<tr><td align=\"right\"><strong>Has the student contacted you for help?</strong></td><td>" + (hasStudentRequestedHelp.HasValue ? (hasStudentRequestedHelp.Value ? "Yes" : "No") : "N/A") + "</td></tr>");
                        sbEmailBody.Append("<tr><td align=\"right\"><strong>Have you contacted the student to discuss your concern?</strong></td><td>" + (hasContactedStudent.HasValue ? (hasContactedStudent.Value ? "Yes" : "No") : "N/A") + "</td></tr>");

                        sbEmailBody.Append("</table>");

                        if (isAutoComplete)
                        {
                            //Email to instructor
                            string instructorEmailBody = "<p>Thank you for sending this alert! This student is a Main Campus student, so it has been forwarded to Steve Neilsen, who is the Director of Retention for Main Campus students.</p>" +
                                                         sbEmailBody.ToString();


                            EmailService.Message instructorMessage = new EmailService.Message
                            {
                                From = new EmailService.EmailAddress { Address = "opsadvising@calbaptist.edu" },
                                Body = instructorEmailBody,
                                IsHtml = true,
                                To = StringToEmailArray(facultyEmail),
                                Bcc = new List<EmailService.EmailAddress> { new EmailService.EmailAddress { Address = "khowlett@calbaptist.edu" } }.ToArray(),
                                Subject = "Retention Alert"
                            };

                            client.QueueEmailComplete(instructorMessage, DateTime.Now);


                            //Email to Main Campus
                            string mainCampusEmailBody = "<p>The below alert has been submitted for a Main Campus student in an OPS class and is being forwarded to you for follow-up. Thank you!</p>" +
                                                         sbEmailBody.ToString();


                            string mainCampuseRententionAlertEmail = (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["MainCampusRetentionAlertEmail"]) ? ConfigurationManager.AppSettings["MainCampusRetentionAlertEmail"] : "sneilsen@calbaptist.edu");

                            EmailService.Message mainCampusMessage = new EmailService.Message
                            {
                                From = new EmailService.EmailAddress { Address = "opsadvising@calbaptist.edu" },
                                Body = mainCampusEmailBody,
                                IsHtml = true,
                                To = StringToEmailArray(mainCampuseRententionAlertEmail),
                                Bcc = new List<EmailService.EmailAddress> { new EmailService.EmailAddress { Address = "khowlett@calbaptist.edu" } }.ToArray(),
                                Subject = "Retention Alert"
                            };

                            client.QueueEmailComplete(mainCampusMessage, DateTime.Now);
                        }
                        else
                        {
                            List<EmailService.EmailAddress> bccs = new List<EmailService.EmailAddress> { new EmailService.EmailAddress { Address = "khowlett@calbaptist.edu" }, new EmailService.EmailAddress { Address = facultyEmail } };

                            //send a copy of the email to admissions if the new student till has a date
                            if (tblStudentAlert.IsNewStudentTillDate.HasValue)
                            {
                                string admissionsRetentionAlertEmail = (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["AdmissionsRetentionAlertEmail"]) ? ConfigurationManager.AppSettings["AdmissionsRetentionAlertEmail"] : "merobinson@calbaptist.edu");

                                bccs.AddRange(StringToEmailArray(admissionsRetentionAlertEmail));
                            }
                            else
                            {
                                //create case in the LEARN app
                                var currentSession = CbuCx.Lib.Academics.AcademicCalendarModel.GetCurrentSemesterWithSubsession();

                                CbuAdvising.Lib.Repository.WorkflowRepository workflowRepo = new CbuAdvising.Lib.Repository.WorkflowRepository();
                                workflowRepo.CreateCaseAndWorkflowInteractions("Retention Alert - " + tblStudentAlert.Concern,
                                                                                cxStudent.CxStudentId,
                                                                                StudentInfoAdvisor.GetStudentAdvisorCXId(cxStudent.CxStudentId, "OPS"),
                                                                                currentSession.Year + "-" + currentSession.Semester + "-" + currentSession.SubSession,
                                                                                sbEmailBody.ToString());
                            }

                            EmailService.Message message = new EmailService.Message
                            {
                                From = new EmailService.EmailAddress { Address = "opsadvising@calbaptist.edu" },
                                Body = "<p>Thank you for sending this alert!  A new retention alert case has been assigned to " + tblStudentAlert.Advisor + " in the LEARN Advising application.</p>" + sbEmailBody.ToString(),
                                IsHtml = true,
                                To = StringToEmailArray("opsadvising@calbaptist.edu"),
                                Bcc = bccs.ToArray(),
                                Subject = "Retention Alert"
                            };

                            client.QueueEmailComplete(message, DateTime.Now);
                        }

                        //Submit Velocify Retention Alert
                        VelocifyServiceRepository sr = new VelocifyServiceRepository();
                        sr.SubmitRetentionAlert(year + "-" + semester + "-" + course + "-" + section, cxStudent.CxStudentId);



                    }
                }

            }
            catch (Exception ex)
            {
                CbuUtilities.Utilities.LogException(ex);
            }

            return saved;
        }

        public List<RetentionAlertInfo> GetRetentionAlertsBySubmittedUser(string userName)
        {
            List<RetentionAlertInfo> infos = new List<RetentionAlertInfo>();

            try
            {
                RetentionAlertRepository r = new RetentionAlertRepository();

                infos = r.GetRetentionAlertsBySubmittedUser(userName);
            }
            catch (Exception ex)
            {
                CbuUtilities.Utilities.LogException(ex);
            }

            return infos;
        }

        public List<RetentionAlertInfo> GetRetentionAlertsByInstructorId(int instructorId)
        {
            List<RetentionAlertInfo> infos = new List<RetentionAlertInfo>();

            try
            {
                RetentionAlertRepository r = new RetentionAlertRepository();

                infos = r.GetRetentionAlertsByInstructorId(instructorId);
            }
            catch (Exception ex)
            {
                CbuUtilities.Utilities.LogException(ex);
            }

            return infos;
        }

        public List<StudentInfo> GetAllRetentionAlerts(string semester, int year)
        {
            List<StudentInfo> infos = new List<StudentInfo>();

            try
            {
                RetentionAlertRepository r = new RetentionAlertRepository();

                infos = r.GetAllRetentionAlerts(semester, year);
            }
            catch (Exception ex)
            {
                CbuUtilities.Utilities.LogException(ex);
            }

            return infos;
        }

        private string GetAdvisor(int studentId)
        {
            string advisor = string.Empty;

            /*List<string> aThroughL = new List<string> { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l" };
            List<string> mThroughZ = new List<string> { "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z" };

            if (subprogram == "MOPS")
            {
                advisor = "Shelley Clow";
            }
            else if (subprogram == "BOPS" || subprogram == "POPS" || subprogram == "COPS" || subprogram == "ADCP")
            {
                if (aThroughL.Contains(studentLastName.ToLower().Substring(0, 1)))
                {
                    advisor = "Katrina Garcia";
                }
                else if (mThroughZ.Contains(studentLastName.ToLower().Substring(0, 1)))
                {
                    advisor = "Brooke Morales";
                }
            }*/

            var advisors = AdvisorLookupRepository.GetAdvisors();
            var studentAdvisorModel = StudentInfoAdvisor.GetStudentInfoAdvisor(studentId, "OPS");

            if (studentAdvisorModel != null && advisors != null && advisors.Count > 0)
            {
                var _a = advisors.FirstOrDefault(x => x.CXAdvisorId == studentAdvisorModel.AdvisorCxId);

                if (_a != null)
                {
                    advisor = _a.FullName;
                }
            }


            return advisor;

        }

        private bool IsAutoCompleteAlert(string subProgram)
        {
            bool isAutoComplete = false;

            switch (subProgram)
            {
                case "TRAD":
                case "MMAN":
                case "ESLG":
                case "ESLU":
                case "VABR":
                    isAutoComplete = true;
                    break;
            }

            return isAutoComplete;
        }

        private DateTime? GetIsNewStudentTillDate(string semester, int year, int cxId)
        {
            DateTime? date = null;

            try
            {
                var s = CbuCx.Lib.Academics.AcademicCalendarModel.GetCurrentSemesterWithSubsession();

                //did this student start this semester and is it still before the 2 week cutoff date (last date to drop)
                if (s.Year == year && s.Semester == semester && s.LastDropDate.Value > DateTime.Now)
                {
                    //now check if they started this subsession
                    if (s.SubSession == "E1")
                    {
                        date = new DateTime(s.LastDropDate.Value.Year, s.LastDropDate.Value.Month, s.LastDropDate.Value.Day, 23, 59, 59);
                    }
                    else if (s.SubSession == "E2")
                    {
                        //check the students course history to see if they are an E2 start
                        if (CbuCx.Lib.User.StudentUser.IsNewSecondSessionStudent(cxId, s.Semester, s.Year))
                        {
                            date = new DateTime(s.LastDropDate.Value.Year, s.LastDropDate.Value.Month, s.LastDropDate.Value.Day, 23, 59, 59);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CbuUtilities.Utilities.LogException(ex);
            }

            return date;
        }

        public List<SemesterModel> GetSemesters()
        {
            RetentionAlertRepository r = new RetentionAlertRepository();

            return r.GetSemesters();
        }
    }
}
