﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CbuRetentionAlert.Lib.Repository;

namespace CbuRetentionAlert.Lib.Models
{
    public class RetentionAlertInfo
    {
        public tblStudentInfo StudentInfo {get;set;}
        public tblStudentAlert Alert { get; set; }
        public List<tblStudentAlertAction> Actions { get; set; }
    }

    public class StudentInfo
    {
        public tblStudentInfo Student { get; set; }
        public List<StudentRetentionAlertInfo> Alerts { get; set; }        
    }

    public class StudentRetentionAlertInfo
    {
        public tblStudentAlert Alert { get; set; }
        public List<tblStudentAlertAction> Actions { get; set; }
    }
}
