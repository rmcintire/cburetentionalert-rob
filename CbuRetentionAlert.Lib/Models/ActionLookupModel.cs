﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CbuRetentionAlert.Lib.Models
{
    public class ActionLookupModel
    {
        public int ActionId { get; set; }
        public string Action { get; set; }
    }
}
