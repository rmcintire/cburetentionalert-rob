﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CbuRetentionAlert.Lib.Repository;
using System.Data.SqlClient;

namespace CbuRetentionAlert.Lib.Models
{
    public class ReportBySessionModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Major { get; set; }
        public string SubProgram { get; set; }
        public string AdmitSemester { get; set; }
        public int AdmitYear { get; set; }
        public string CourseNumber { get; set; }
        public string CourseSection { get; set; }
        public string Semester { get; set; }
        public int Year { get; set; }

        public string Advisor { get; set; }
        public string Status { get; set; }
        public string Concern { get; set; }
        public bool? IsBlackboardAdminGenerated { get; set; }
        public DateTime Created { get; set; }
        public DateTime? FirstRespondedOn { get; set; }
        public DateTime? CompletedOn { get; set; }
        public string Resolution { get; set; }
        public string TimeToResponse
        {
            get
            {
                string temp = string.Empty;

                if (FirstRespondedOn.HasValue)
                {
                    TimeSpan timeSpan = FirstRespondedOn.Value.Subtract(Created);

                    temp = string.Format("{0:%d} day(s) {0:%h} hour(s) {0:%m} minute(s) {0:%s} second(s)", timeSpan);
                }

                return temp;
            }
        }
        public string TimeToCompletion
        {
            get
            {
                string temp = string.Empty;

                if (CompletedOn.HasValue)
                {
                    TimeSpan timeSpan = CompletedOn.Value.Subtract(Created);

                    temp = string.Format("{0:%d} day(s) {0:%h} hour(s) {0:%m} minute(s) {0:%s} second(s)", timeSpan);
                }

                return temp;
            }
        }


        public static List<ReportBySessionModel> GetReportData(string session, int year)
        {
            var data = new List<ReportBySessionModel>();

            try
            {

                using (var context = new CbuRetentionAlertEntities())
                {
                    StringBuilder sbCommand = new StringBuilder();
                    sbCommand.Append("SELECT ");
                    sbCommand.Append("si.FirstName,  ");
                    sbCommand.Append("si.LastName,  ");
                    sbCommand.Append("si.Major,  ");
                    sbCommand.Append("si.SubProgram, ");
                    sbCommand.Append("si.AdmitSemester, ");
                    sbCommand.Append("si.AdmitYear, ");
                    sbCommand.Append("sa.CourseNumber,  ");
                    sbCommand.Append("sa.CourseSection,  ");
                    sbCommand.Append("sa.Semester,  ");
                    sbCommand.Append("sa.Year,  ");
                    sbCommand.Append("sa.Advisor,  ");
                    sbCommand.Append("sa.Status,  ");
                    sbCommand.Append("sa.Concern,  ");
                    sbCommand.Append("sa.IsBlackboardAdminGenerated, ");
                    sbCommand.Append("sa.Created, ");
                    sbCommand.Append("(SELECT TOP 1 Created from tblStudentAlertAction where StudentAlertId = sa.Id and Comment not like '%New alert submitted by%') AS 'FirstRespondedOn', ");
                    sbCommand.Append("(SELECT TOP 1 Created from tblStudentAlertAction where StudentAlertId = sa.Id and ActionType = 'Complete') AS 'CompletedOn', ");
                    sbCommand.Append("(SELECT TOP 1 Comment from tblStudentAlertAction where StudentAlertId = sa.Id and ActionType = 'Complete' ORDER BY Created desc) AS 'Resolution' ");
                    sbCommand.Append("FROM tblStudentAlert sa ");
                    sbCommand.Append("inner join tblStudentInfo si ");
                    sbCommand.Append("ON sa.StudentInfoId = si.Id ");
                    sbCommand.Append("WHERE sa.Semester = @Semester ");
                    sbCommand.Append("AND sa.Year = 2013 ");
                    sbCommand.Append("order by si.LastName, si.FirstName, sa.AlertPriority  ");

                    var parameters = new object[]{new SqlParameter{ DbType = System.Data.DbType.String, ParameterName = "@Semester", Value = session},
                                              new SqlParameter{ DbType = System.Data.DbType.Int32, ParameterName = "@Year", Value = year}
                                             };

                    var result = context.ExecuteStoreQuery<ReportBySessionModel>(sbCommand.ToString(), parameters);

                    data = result.ToList<ReportBySessionModel>();
                }
            }
            catch (Exception ex)
            {
                CbuUtilities.Utilities.LogException(ex);
            }

            return data;
        }
    }
}
