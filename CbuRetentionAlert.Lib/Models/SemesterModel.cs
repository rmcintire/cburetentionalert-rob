﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CbuRetentionAlert.Lib.Models
{
    public class SemesterModel
    {
        public string Semester { get; set; }
        public int Year { get; set; }
    }
}
