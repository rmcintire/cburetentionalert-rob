﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CbuRetentionAlert.Lib.Models
{
    public class StatusLookupModel
    {
        public int StatusId { get; set; }
        public string Status { get; set; }
        public int Priority { get; set; }
    }
}
