﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CbuRetentionAlert.Lib.Models
{
    public class AdvisorModel
    {
        public int AdvisorId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string AdUserName { get; set; }
        public string FullName { get { return FirstName + " " + LastName; } }
        public int CXAdvisorId { get; set; }
    }
}
