﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CbuRetentionAlert.Lib.Enum
{
    public enum AlertPriorityEnum
    {
        New = 0,
        PendingFollowUp = 1,
        PendingContact = 3,
        Completed = 10
    }
}
