﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CbuRetentionAlert.Lib.Models;

namespace CbuRetentionAlert.Lib.Repository
{
    public class AdvisorLookupRepository
    {
        public static List<AdvisorModel> GetAdvisors()
        {
            List<AdvisorModel> advisors = new List<AdvisorModel>();

            try
            {
                using (var context = new CbuRetentionAlertEntities())
                {
                    var _advisors = (from a in context.tblAdvisorLookups
                                     where a.Active == true
                                     select new { a.AdUserName, a.FirstName, a.Id, a.LastName, a.AdvisorId }).ToList();

                    foreach (var advisor in _advisors)
                    {
                        advisors.Add(new AdvisorModel
                        {
                            AdUserName = advisor.AdUserName,
                            AdvisorId = advisor.Id,
                            FirstName = advisor.FirstName,
                            LastName = advisor.LastName,
                            CXAdvisorId = (advisor.AdvisorId.HasValue ? advisor.AdvisorId.Value : -1)
                        });
                    }
                }
            }
            catch
            {
                
                throw;
            }

            return advisors;
        }
    }
}
