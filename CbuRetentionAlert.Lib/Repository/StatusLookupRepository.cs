﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CbuRetentionAlert.Lib.Models;

namespace CbuRetentionAlert.Lib.Repository
{
    public class StatusLookupRepository
    {
        public static List<StatusLookupModel> GetStatuses()
        {
            List<StatusLookupModel> statuses = new List<StatusLookupModel>();

            try
            {
                using (var context = new CbuRetentionAlertEntities())
                {
                    var _statuses = (from s in context.tblStatusLookups
                                where s.Active
                                select new { StatusId = s.Id, Status = s.Status, Priority = s.Priority }).ToList();

                    foreach (var status in _statuses)
                    {
                        statuses.Add(new StatusLookupModel { Priority = status.Priority, Status = status.Status, StatusId = status.StatusId });
                    }
                }
            }
            catch
            { 
                throw;
            }

            return statuses;
        }
    }
}
