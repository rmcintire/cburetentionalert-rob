﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CbuRetentionAlert.Lib.Models;

namespace CbuRetentionAlert.Lib.Repository
{
    public class ActionLookupRepository
    {
        public static List<ActionLookupModel> GetActions()
        {
            List<ActionLookupModel> actions = new List<ActionLookupModel>();

            try
            {
                using (var context = new CbuRetentionAlertEntities())
                {
                    var _actions = (from a in context.tblActionLookups
                                    where a.Active == true
                                    select new { a.Action, a.Id }).ToList();

                    foreach (var action in _actions)
                    {
                        actions.Add(new ActionLookupModel { Action = action.Action, ActionId = action.Id });
                    }
                }
            }
            catch
            {
                
                throw;
            }

            return actions;
        }
    }
}
