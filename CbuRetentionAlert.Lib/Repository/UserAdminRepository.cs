﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CbuRetentionAlert.Lib.Repository
{
    public class UserAdminRepository
    {
        public bool IsUserAdmin(string userName)
        {
            bool isAdmin = false;

            try
            {
                using (var context = new CbuRetentionAlertEntities())
                {
                    var user = (from u in context.tblUserAdmins
                                where u.AdUserName == userName && u.Active == true
                                select u).FirstOrDefault();

                    if (user != null)
                    {
                        isAdmin = true;
                    }
                }
            }
            catch
            {
                
                throw;
            }

            return isAdmin;
        }
    }
}
