﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CbuRetentionAlert.Lib.Models;

namespace CbuRetentionAlert.Lib.Repository
{
    public class RetentionAlertRepository
    {
        public bool SaveRetentionAlert(tblStudentInfo student, tblStudentAlert alert, tblStudentAlertAction action)
        {
            bool saved = false;

            try
            {
                using (var context = new CbuRetentionAlertEntities())
                {
                    var _student = (from s in context.tblStudentInfoes
                                    where s.StudentId == student.StudentId
                                    select s).FirstOrDefault();

                    if (_student == null)
                    {
                        context.tblStudentInfoes.AddObject(student);
                        context.SaveChanges();

                        alert.StudentInfoId = student.Id;

                        context.tblStudentAlerts.AddObject(alert);
                        context.SaveChanges();

                        action.StudentAlertId = alert.Id;
                        context.tblStudentAlertActions.AddObject(action);
                        context.SaveChanges();
                    }
                    else
                    {
                        //update student and enter new alert
                        _student.EmailAddress = student.EmailAddress;
                        _student.FirstName = student.FirstName;
                        _student.LastName = student.LastName;
                        _student.Major = student.Major;
                        _student.Modified = DateTime.Now;
                        _student.PhoneNumber = student.PhoneNumber;
                        _student.Prefix = student.Prefix;
                        _student.StudentId = student.StudentId;
                        _student.SubProgram = student.SubProgram;
                        _student.Suffix = student.Suffix;
                        _student.AdmitSemester = student.AdmitSemester;
                        _student.AdmitYear = student.AdmitYear;

                        alert.StudentInfoId = _student.Id;

                        context.tblStudentAlerts.AddObject(alert);
                        context.SaveChanges();

                        action.StudentAlertId = alert.Id;
                        context.tblStudentAlertActions.AddObject(action);
                        context.SaveChanges();
                    }

                    saved = true;
                }
            }
            catch
            {
                throw;
            }

            return saved;
        }

        public bool UpdateStudentAdmitInfo(string admitSemester, int admitYear, string cxId)
        {
            bool saved = false;

            try
            {
                using (var context = new CbuRetentionAlertEntities())
                {
                    var _student = (from s in context.tblStudentInfoes
                                    where s.StudentId == cxId
                                    select s).FirstOrDefault();

                    if (_student != null)
                    {
                        //update student and enter new alert
                        _student.AdmitSemester = admitSemester;
                        _student.AdmitYear = admitYear;

                        context.SaveChanges();
                    }

                    saved = true;
                }
            }
            catch
            {
                throw;
            }

            return saved;
        }

        public List<RetentionAlertInfo> GetRetentionAlertsBySubmittedUser(string userName)
        {
            var infos = new List<RetentionAlertInfo>();

            try
            {
                using (var context = new CbuRetentionAlertEntities())
                {
                    List<tblStudentAlert> alerts = (from a in context.tblStudentAlerts
                                                    where a.CreatedBy == userName
                                                    select a).ToList();

                    foreach (var alert in alerts)
                    {
                        var student = (from s in context.tblStudentInfoes
                                       where s.Id == alert.StudentInfoId
                                       select s).FirstOrDefault();

                        infos.Add(new RetentionAlertInfo { Alert = alert, StudentInfo = student });
                    }
                }
            }
            catch
            {
                throw;
            }

            return infos;
        }

        public List<RetentionAlertInfo> GetRetentionAlertsByInstructorId(int instructorId)
        {
            var infos = new List<RetentionAlertInfo>();

            try
            {
                using (var context = new CbuRetentionAlertEntities())
                {
                    List<tblStudentAlert> alerts = (from a in context.tblStudentAlerts
                                                    where a.InstructorId == instructorId
                                                    select a).ToList();

                    foreach (var alert in alerts)
                    {
                        var student = (from s in context.tblStudentInfoes
                                       where s.Id == alert.StudentInfoId
                                       select s).FirstOrDefault();

                        infos.Add(new RetentionAlertInfo { Alert = alert, StudentInfo = student });
                    }
                }
            }
            catch
            {
                throw;
            }

            return infos;
        }

        public List<StudentInfo> GetAllRetentionAlerts(string semester, int year)
        {
            var infos = new List<StudentInfo>();

            try
            {
                using (var context = new CbuRetentionAlertEntities())
                {
                    var alertInfos = new List<StudentRetentionAlertInfo>();

                    var alerts = (from a in context.tblStudentAlerts
                                  where a.Semester == semester && a.Year == year
                                  orderby a.AlertPriority, a.FollowUpDate ascending, a.Created ascending
                                  select a).ToList();

                    //get all the actions for these alerts
                    foreach (var alert in alerts)
                    {
                        /* var actions = (from a in context.tblStudentAlertActions
                                        where a.StudentAlertId == alert.Id
                                        orderby a.Created descending
                                        select a).ToList();
                         */

                        alertInfos.Add(new StudentRetentionAlertInfo { Alert = alert, Actions = null });
                    }



                    var studentIds = (from a in alertInfos.AsEnumerable()
                                      select new { a.Alert.StudentInfoId }).Distinct().ToList();

                    List<int> ids = new List<int>();

                    foreach (var studentId in studentIds)
                    {
                        ids.Add(studentId.StudentInfoId);
                    }

                    var studentInfos = (from s in context.tblStudentInfoes
                                        where ids.Contains(s.Id)
                                        select s).ToList();

                    Dictionary<int, bool> students = new Dictionary<int, bool>();

                    foreach (var alertInfo in alertInfos)
                    {
                        if (!students.ContainsKey(alertInfo.Alert.StudentInfoId))
                        {
                            students.Add(alertInfo.Alert.StudentInfoId, true);
                            infos.Add(new StudentInfo { Student = studentInfos.Where(x => x.Id == alertInfo.Alert.StudentInfoId).FirstOrDefault(), Alerts = alertInfos.FindAll(y => y.Alert.StudentInfoId == alertInfo.Alert.StudentInfoId) });
                        }
                    }

                    //for each alert get the student
                    /*foreach (var alert in alertInfos)
                    {
                        var student = (from s in context.tblStudentInfoes
                                       where s.Id == alert.Alert.StudentInfoId
                                       select s).FirstOrDefault();

                        var _student = (from s in infos
                                        where s.Student.Id == student.Id
                                        select s).FirstOrDefault();
                        

                        if (!students.ContainsKey(student.Id))
                        {
                            students.Add(student.Id, true);
                            infos.Add(new StudentInfo { Student = student, Alerts = alertInfos.FindAll(y => y.Alert.StudentInfoId == student.Id) });
                        }

                        if (_student == null)
                        {
                            infos.Add(new StudentInfo { Student = student, Alerts = alertInfos.FindAll(y => y.Alert.StudentInfoId == student.Id) });
                        }
                        
                    }*/

                }
            }
            catch
            {
                throw;
            }

            return infos;
        }

        public List<tblStudentInfo> GetStudentInfos()
        {
            var infos = new List<tblStudentInfo>();

            try
            {
                using (var context = new CbuRetentionAlertEntities())
                {

                    infos = (from s in context.tblStudentInfoes
                             select s).ToList();


                }
            }
            catch
            {
                throw;
            }

            return infos;
        }

        public RetentionAlertInfo GetRetentionAlertById(int id)
        {
            var alert = new RetentionAlertInfo();

            try
            {
                using (var context = new CbuRetentionAlertEntities())
                {
                    alert.Alert = (from a in context.tblStudentAlerts
                                   where a.Id == id
                                   select a).FirstOrDefault();


                    alert.StudentInfo = (from s in context.tblStudentInfoes
                                         where s.Id == alert.Alert.StudentInfoId
                                         select s).FirstOrDefault();

                    alert.Actions = (from a in context.tblStudentAlertActions
                                     where a.StudentAlertId == alert.Alert.Id
                                     select a).ToList();
                }
            }
            catch
            {
                throw;
            }

            return alert;
        }

        public bool UpdateStudentOtherPhone(int id, string phone)
        {
            bool saved = false;

            try
            {
                using (var context = new CbuRetentionAlertEntities())
                {

                    var studentInfo = (from s in context.tblStudentInfoes
                                       where s.Id == id
                                       select s).FirstOrDefault();

                    if (studentInfo != null)
                    {
                        studentInfo.OtherPhone = phone;
                        studentInfo.Modified = DateTime.Now;
                        context.SaveChanges();

                        saved = true;
                    }

                }
            }
            catch
            {
                throw;
            }

            return saved;
        }

        public bool SaveAlertDetails(int alertId, int advisorId)
        {
            bool saved = false;

            try
            {
                using (var context = new CbuRetentionAlertEntities())
                {

                    var advisor = (from a in context.tblAdvisorLookups
                                   where a.Id == advisorId
                                   select a).FirstOrDefault();

                    //var status = (from s in context.tblStatusLookups
                    //              where s.Id == statusId
                    //              select s).FirstOrDefault();

                    if (advisor != null)
                    {
                        var alert = (from a in context.tblStudentAlerts
                                     where a.Id == alertId
                                     select a).FirstOrDefault();

                        if (alert != null)
                        {
                            alert.Advisor = advisor.FirstName + " " + advisor.LastName;
                            //alert.Status = status.Status;
                            //alert.AlertPriority = status.Priority;

                            context.SaveChanges();

                            saved = true;
                        }
                    }
                }
            }
            catch
            {
                throw;
            }

            return saved;
        }

        public bool SaveAlertAction(int alertId, int actionId, string comment, DateTime? followUpDate, string createdBy)
        {
            bool saved = false;

            try
            {
                using (var context = new CbuRetentionAlertEntities())
                {

                    var actionType = (from a in context.tblActionLookups
                                      where a.Id == actionId
                                      select a).FirstOrDefault();

                    var alert = (from a in context.tblStudentAlerts
                                 where a.Id == alertId
                                 select a).FirstOrDefault();


                    if (actionType != null)
                    {
                        //if there is a follow update automatically set the priority using the follow-up status priority and followup date
                        if (actionType.Action == "Complete")
                        {
                            var status = (from s in context.tblStatusLookups
                                          where s.Id == 6 // 6 being the id for the completed status
                                          select s).FirstOrDefault();

                            if (alert != null && status != null)
                            {
                                alert.Status = status.Status;
                                alert.AlertPriority = status.Priority;
                                alert.FollowUpDate = null;
                            }

                        }
                        else if (actionType.Action != "Comment")
                        {
                            if (followUpDate.HasValue)
                            {
                                if (alert != null)
                                {
                                    alert.FollowUpDate = followUpDate.Value;
                                }
                            }
                        }


                        if (actionType.Action != "Complete")
                        {
                            var status = (from s in context.tblStatusLookups
                                          where s.Id == 5 // 6 being the id for the pending status
                                          select s).FirstOrDefault();

                            //set the status to pending
                            if (alert != null && status != null)
                            {
                                alert.Status = status.Status;
                                alert.AlertPriority = status.Priority;
                            }
                        }

                        //save the action
                        var tblAction = new tblStudentAlertAction
                        {
                            ActionType = actionType.Action,
                            Comment = (!string.IsNullOrEmpty(comment) ? comment : string.Empty),
                            Created = DateTime.Now,
                            CreatedBy = createdBy,
                            StudentAlertId = alertId
                        };


                        if (followUpDate.HasValue)
                        {
                            tblAction.FollowUpDate = followUpDate.Value;
                        }
                        else
                        {
                            tblAction.FollowUpDate = null;
                        }

                        context.tblStudentAlertActions.AddObject(tblAction);
                        context.SaveChanges();

                        saved = true;
                    }
                }
            }
            catch
            {
                throw;
            }

            return saved;
        }

        public bool SaveInstructorAlertAction(int alertId, string action, string comment, string createdBy)
        {
            bool saved = false;

            try
            {
                using (var context = new CbuRetentionAlertEntities())
                {
                    var alert = (from a in context.tblStudentAlerts
                                 where a.Id == alertId
                                 select a).FirstOrDefault();



                    //if there is a follow update automatically set the priority using the follow-up status priority and followup date
                    if (action == "Complete")
                    {
                        var status = (from s in context.tblStatusLookups
                                      where s.Id == 6 // 6 being the id for the completed status
                                      select s).FirstOrDefault();

                        if (alert != null && status != null)
                        {
                            alert.Status = status.Status;
                            alert.AlertPriority = status.Priority;
                            alert.FollowUpDate = null;
                        }

                    }
                    else if (action == "Comment")
                    {
                        //do not changes the status for instructors
                        /*var status = (from s in context.tblStatusLookups
                                      where s.Id == 5 // 5 being the id for the pending status
                                      select s).FirstOrDefault();

                        //set the status to pending
                        if (alert != null && status != null)
                        {
                            alert.Status = status.Status;
                            alert.AlertPriority = status.Priority;
                        }
                        */
                    }
                    else if (action == "Comment Leave Completed")
                    {
                        var status = (from s in context.tblStatusLookups
                                      where s.Id == 6 // 6 being the id for the completed status
                                      select s).FirstOrDefault();

                        if (alert != null && status != null)
                        {
                            alert.Status = status.Status;
                            alert.AlertPriority = status.Priority;
                            alert.FollowUpDate = null;
                        }
                    }
                    else if (action == "Comment Re-Open")
                    {
                        var status = (from s in context.tblStatusLookups
                                      where s.Id == 7 // 7 being the id for the re-opened status
                                      select s).FirstOrDefault();

                        //set the status to pending
                        if (alert != null && status != null)
                        {
                            alert.Status = status.Status;
                            alert.AlertPriority = status.Priority;
                        }
                    }

                    //save the action
                    var tblAction = new tblStudentAlertAction
                    {
                        ActionType = action,
                        Comment = (!string.IsNullOrEmpty(comment) ? comment : string.Empty),
                        Created = DateTime.Now,
                        CreatedBy = createdBy,
                        StudentAlertId = alertId,
                        FollowUpDate = null
                    };

                    context.tblStudentAlertActions.AddObject(tblAction);
                    context.SaveChanges();

                    saved = true;
                }
            }
            catch
            {
                throw;
            }

            return saved;
        }

        public List<SemesterModel> GetSemesters()
        {
            List<SemesterModel> sems = new List<SemesterModel>();

            try
            {
                using (var context = new CbuRetentionAlertEntities())
                {
                    var alerts = (from a in context.tblStudentAlerts
                                  orderby a.Created descending
                                  select a).ToList();

                    var _sems = (from a in alerts.AsEnumerable()
                                 select new { a.Year, a.Semester }).Distinct().ToList();

                    foreach (var s in _sems)
                    {
                        sems.Add(new SemesterModel { Semester = s.Semester, Year = s.Year });
                    }
                }
            }
            catch (Exception ex)
            {
                CbuUtilities.Utilities.LogException(ex);
            }

            return sems;
        }
    }
}
